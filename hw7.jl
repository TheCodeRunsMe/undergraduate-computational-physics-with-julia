using Polynomials
using LinearAlgebra
using Plots

a = open("einstein.txt", "r") do io
    read(io,String)
end

b = split(a,"\r\n")
c = []; d = []

for i in b
    e,f = split(i," ")
    append!(c,parse(Int, e));append!(d,parse(Int, f))
end

lambdacoef = [prod([1 / (i - j) for j in c if j != i]) for i in c]
lambda = [Poly([j for j in c if j != i]) for i in c]
f = dot(lambdacoef, lambda)

pyplot()
plot(c,d); display(plot!(c,f.(c)))
sleep(20)

g = [54 sum(c); sum(c) sum(c .^ 2)] \ [sum(d); dot(x,y)]
plot(c,d); display(plot!(c, polyval(Poly(g),c)))
sleep(20)

h = sum(c); h2 = sum(c.^2); h3= sum(c.^3); h4 = sum(c.^4);h5 = sum(c.^5); h6 = sum(c.^6) ;
l = [54 h1 h2; h1 h2 h3; h2 h3 h4] \ [sum(d); sum(dot(c.^2, d)); sum(dot(c.^2, d))]
plot(c,d); display(plot!(c, polyval(Poly(l),c)))
sleep(20)

k = ([54 h1 h2 h3; h1 h2 h3 h4; h2 h3 h4 h5; h3 h4 h5 h6] \
[sum(d); sum(dot(c.^2, d)); dot(c.^2, d); dot(c.^3, d)])
plot(c,d); display(plot!(c, polyval(Poly(k),c)))
sleep(20)

