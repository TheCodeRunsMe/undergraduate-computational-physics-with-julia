const n = 4.
vars = Dict{String, Array{Float64}}()

for i in ["mass", "x", "y"]
    println("Enter the ", i, " values one by one sequentially:")
    vars[i] = [parse(Float64, readline()) for j in 1:n]
end

massvec, xvec, yvec = vars["mass"], vars["x"], vars["y"]
com(m::Array{Float64}, x::Array{Float64}, y::Array{Float64}) = m'x/sum(m), m'y/sum(m)
println("Center of mass:", com(massvec, xvec, yvec))

