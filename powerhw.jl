 using Plots

 const a = [0 11 -5; -2 17 -7; -4 26 -10]
 const tol = 1e-5
 err = 30.
 x = fill(1,3)
 i = 1
 eigv = []
 while err > tol
     global x = a * x
     append!(eigv, maximum(x))
     x = x ./ eigv[i]
     length(eigv) > 1 ? (global err = abs(eigv[i] - eigv[i - 1])) : nothing
     global i += 1
 end
 print("Dominant eigen value is ", eigv[i - 1], " #iterations is ", i - 1)
 pyplot()
 display(plot(eigv))
 # change that value to prolonge display duration of the plot
 sleep(10)
