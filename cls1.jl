using Random

const N = Int(1e6)
rand!(RandomDevice(), v)

function loopy()
    sumy = 0
    for i in v
        i < .5 && sumy += 1
    end
    sumy
end

vectory() =  sum(v .> .5)

@time loopy()
@time vectory()

