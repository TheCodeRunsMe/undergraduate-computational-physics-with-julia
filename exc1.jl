using JLD

v1 = collect(1:100)
v2 = rand(100)
v3 = [v1 v2]

jldopen("randomexercise.txt", "w") do io
    write(io, "veri", v3)
end
