using PyPlot

f(x) = exp(-x)

x = collect(3:.001:5)
a = .001 / 2 * sum(f.(x)) + .001*sum(f.(x[2:end - 1]))

b = 2 * sum(f.(2*rand(10) .+ 3)) / 10
c = 2 * sum(f.(2*rand(100) .+ 3)) / 100
d = 2 * sum(f.(2*rand(1000) .+ 3)) / 1000
e = 2 * sum(f.(2*rand(10000) .+ 3)) / 10000

display(plot(10 .^ (1:4), [b; c; d; e]))
sleep(20)

ga = [1 1] * f.([-0.5773502692; .5773502692])
gb = [.5555555556 .5555555556 .8888888888] * f.([.7745966692; -.7745966692; 0])
gc = ([.3478548451 .3478548451 .6521451549 .6521451549] *
f.([.8611363116; -.8611363116; .3399810436; -.3399810436]))
gd = ([.2369268851 .2369268851 .4786286705 .4786286705 .5688888888] *
f.([.9061798459; -.9061798459; .5384693101; -.5384693101; 0]))

print(ga,gb,gc,gd)
