using PyPlot, JLD, Statistics

v3 = jldopen("randomexercise.txt", "r") do io
    read(io, "veri")
end
v1, v2 = v3[:, 1], v3[:, 2]
display(plot(v1 ,v2))
sleep(5)
findeverything(v::Array{Float64}) = max(v...), min(v...), mean(v), std(v)
println(findeverything(v2))
