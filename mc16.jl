using Plots; pyplot()
const lambda= 0.0008675; n = [100]; s = 1

while n[s] != 0
    d = 0;
    for i = 1:n[s]
        rand() < lambda && (d += 1)
    end
    append!(n,n[s] - d)
    global s += 1
end
plot(1:s,n)
