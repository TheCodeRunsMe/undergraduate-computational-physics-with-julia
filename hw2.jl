using LinearAlgebra

const esp = 1e-5 # julia has a function eps returning machine epsilon
print("Enter the dim of the matrix: ")
a = parse(Int, chomp(readline()))
m = rand(a,a) * 290 .+ 10
mult = m * inv(m)

#this is the way requested in the hw
function isitunit(mat::Array{Float64,2})
    n = length(mat)
    correctelements = 0
    for i in 1:size(mat,1)
        for j in 1:size(mat,2)
            if i == j
                -esp < mat[i,i] - 1 < esp && (correctelements += 1)
            else
                -esp < mat[i,j] < esp && (correctelements += 1)
            end
        end
    end
    if correctelements == n
        1
    else
        0
    end
end

# this is an idiomatic way to code in julia the same functions as the one above
#isitunit(mat::Array{Float64,2}) = sum(Int.(-esp .< mat - I .< esp)) == length(mat)

if Bool(isitunit(mult))
    print("it is unit")
else
    print("it is not unit")
end

