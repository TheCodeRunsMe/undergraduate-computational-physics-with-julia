using Plots

o, h, h2o = fill(10000,10000), fill(10000,10000), fill(0,10000)

function bond(o::Array{Int64,1}, h::Array{Int64,1}, h2o::Array{Int64,1}, i::Int64, b::Bool)
    b && (h[i] = h[i-1] - 2; o[i] = o[i-1] - 1; h2o[i] = h2o[i-1] + 1)
    b || (h[i] = h[i-1]; o[i] = o[i-1]; h2o[i] = h2o[i-1])
end

for i = 2:10000
    rand() < (h[i-1] / o[i-1]) ? bond(h,o,h2o,i,true) : bond(h,o,h2o,i,false)
end

pyplot(); i = 1:10000; p = plot(i,o); plot!(i,h); plot!((i,h2o)): display(p); sleep(1111)
