i = 1
while true
    b = rand(1:5, 100)
    if rand() < .3
        if rand() < .15
            print("Expelled at the year ")
            break
        else
            v = fill(2, 100); v[1:10] = b[1:10]
            sum(Int.(v .== b)) > 65 ? break : global i += 1
        end
    else
        sum(Int.(fill(2,100) .== b)) > 65 ? break : global i += 1
    end
end

println(i)
